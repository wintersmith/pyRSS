"""
    _fdb.py - provides DB methods for utilising FoundationDB.  

"""
import os
import fdb


def tupleToKey( baseTuple ):
    return '|'.join( str( indivTuple ) for indivTuple in baseTuple )

class DatabaseDrive( object ):

    _dbConn = None

    def __init__( self, dbConfig ):
        self._dbConfig = dbConfig

    def connect( self ):
        fdb.api_version( self._dbConfig[ 'apiVersion' ] )
        self._dbConn = fdb.open()
        self._rootDB = fdb.directory.create_or_open( self._dbConn, ( 'rss', ) )