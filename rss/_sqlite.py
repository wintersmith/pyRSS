"""
    _sqlite.py - provides DB methods for utilising SQLite3.  

"""
import os
import sys
import sqlite3


rssSchema = """CREATE TABLE user (
	id INTEGER NOT NULL,
	email VARCHAR(120),
	role SMALLINT,
	last_seen DATETIME,
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX ix_user_email ON user (email);
CREATE TABLE opml (
	id INTEGER NOT NULL,
	userID INTEGER,
	feedText VARCHAR(255),
	feedTitle VARCHAR(255),
	feedHTML VARCHAR(255),
	feedXML VARCHAR(255),
	feedLogin VARCHAR(64),
	feedPass VARCHAR(128),
	lastUpdated DATETIME,
	PRIMARY KEY (id)
);"""

def dict_factory( cursor, row ):
    return dict( ( cursor.description[ idx ][ 0 ], value) for idx, value in enumerate( row ) )

class DatabaseDrive( object ):

    _dbConn = None

    def __init__( self, dbConfig ):
        self._dbConfig = dbConfig

    def connect( self ):
        try:
            self._dbConn =  sqlite3.connect( self._dbConfig[ 'Schema' ] )
            self._dbConn.row_factory = dict_factory
            self._checkExists()
        except sqlite3.OperationalError as errMsg:
            pass

    def _checkExists( self ):
        if not self._dbConn is None:
            doesExist = "SELECT name FROM sqlite_master WHERE type='table' AND name='opml'"
            if not self._dbConn.execute( doesExist ).fetchone():
                with self._dbConn:
                    for indivCmd in rssSchema.split( ';' ):
                        self._dbConn.execute( '{};'.format( indivCmd ) )

    def addFeed( self, userId: int, feedXML: str, feedTitle: str ):
        if not self._dbConn is None:
            with self._dbConn:
                self._dbConn.execute( "INSERT INTO opml ( userId, feedXML, feedTitle ) VALUES ( ?, ?, ? )", ( userId, feedXML, feedTitle ) )

    def listFeeds( self, userId ):
        if not self._dbConn is None:
            with self._dbConn:
                for feedRes in self._dbConn.execute( "SELECT id, feedXML from opml where userId = ?", ( userId, ) ).fetchall():
                    yield feedRes

    def updateFeed( self, feedId, feedDets ):
        if not self._dbConn is None:
            with self._dbConn:
                sqlToRun, valueList = self._generateUpdate( 'opml', 'id', feedId, feedDets )
                self._dbConn.execute( sqlToRun, valueList )

    def _generateUpdate( self, tableName, keyName, idV, fieldV ):
            valueList = []
            for keyIdx, keyV in enumerate( fieldV ):
                sqlToRun += '{} = ?{}'.format( keyV, ', ' if keyIdx + 1 < len( fieldV.keys() ) else ' ' )
                valueList.append( fieldV[ keyV ] )
            sqlToRun = 'UPDATE {} SET {} WHERE {} = ?;'.format( tableName, sqlToRun, keyName )
            valueList.append( idV )

            return sqlToRun, tuple( valueList )


def dictSearch( resDict: dict, listDict: list ) -> bool:
    retValue = True
    for resSkill in resDict:
        if any( cSkill[ 'skillID' ] == resSkill[ 'skillID' ] for cSkill in listDict ):
            for cSkill in listDict:
                if resSkill[ 'skillRating' ] < cSkill[ 'skillRating' ]:
                    retValue = False
        else:
            retValue = False
            
    return retValue
