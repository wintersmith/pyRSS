import feedparser

from rss import database

def checkFeed( feedURL ):
    rssFeed = feedparser.parse( feedURL )
    for indivPost in rssFeed.entries:
        print( indivPost[ 'title' ] )
        print( indivPost[ 'published' ] )
        print( indivPost[ 'updated' ] )

def listFeeds( dbConn, userId ):
    for indivFeed in dbConn.listFeeds( userId ):
        yield indivFeed

def updateFeed( dbConn, userId, **feedUpdate ):
    dbConn.updateFeed( userId, feedUpdate )
