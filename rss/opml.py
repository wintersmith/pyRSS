import json
from xml.etree import ElementTree

from rss import database

def jsonToDict( postValues ):
    dictValues = None
    try:
        dictValues = postValues.get_json()
    except Exception as errMsg:
        postValues

    return dictValues

def parseOPML( opmlString ):
    opmlTree = ElementTree.fromstring( opmlString )
    for indivNode in opmlTree.findall( './/outline' ):
        yield indivNode.attrib.get( 'text' ), indivNode.attrib.get( 'title' ), indivNode.attrib.get( 'xmlUrl' ), indivNode.attrib.get( 'htmlUrl' )

def parseXML( opmlXML ):
    opmlTree = ElementTree.fromstring( opmlXML )
    for node in opmlTree.findall( './/outline' ):
            print( node.attrib.get( 'text' ), node.attrib.get( 'title' ), node.attrib.get( 'xmlUrl' ), node.attrib.get( 'htmlUrl' ) )