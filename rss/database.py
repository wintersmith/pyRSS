"""
    database.py - Library that provides functions to enable the app to be db agnostic.  Expects all results from driver functions to be dicts.
"""
from __future__ import print_function
import os
import sys
import types
import importlib

envConfig = importlib.import_module( 'config.{}'.format( os.getenv( 'RSS_ENV', 'dev' ) ) )
dbDriver = importlib.import_module( '._%s' % envConfig.dbConfig[ 'dbType' ], 'rss' )
dbClass = getattr( dbDriver,'DatabaseDrive' )

def getDB():
    dbConn = getattr( g, '_database', None )
    if dbConn is None:
        dbObj = Database( envConfig.dbConfig[ 'dbHost' ], envConfig.dbConfig[ 'dbPort' ], envConfig.dbConfig[ 'dbSchema' ], 
                                                                dbUser = envConfig.dbConfig[ 'dbUser' ], dbPass = envConfig.dbConfig[ 'dbPass' ] )
        dbConn = g._database = dbObj
        dbConn.connect()

    return dbConn
    
class Database( object ):

    _dbConfig = {}

    def __init__( self, dbHost, dbPort, dbSchema, **kwArgs ):
        hostConfig = []
        if type( dbHost ) is list:
            for listIndex, indivHost in enumerate( dbHost ):
                hostConfig.append( ( indivHost, dbPort[ listIndex] if type( dbPort ) is list else dbPort ) )
        else:
            hostConfig.append( ( dbHost, dbPort[ 0 ] if type( dbPort ) is list else dbPort ) )

        self._dbConfig[ 'Hosts' ] = hostConfig
        self._dbConfig[ 'User' ] = kwArgs[ 'dbUser' ] if 'dbUser' in kwArgs else None
        self._dbConfig[ 'Passwd' ] = kwArgs[ 'dbPass' ] if 'dbPass' in kwArgs else None
        self._dbConfig[ 'Schema' ] = dbSchema
        if 'apiVersion' in kwArgs:
            self._dbConfig [ 'Api' ] = kwArgs[ 'apiVersion' ]

        self._dbDriver = dbClass( self._dbConfig )

    def connect( self ):
        self._dbDriver.connect()

    def addFeed( self, userId, feedXML, feedTitle ):
        self._dbDriver.addFeed( userId, feedXML, feedTitle )
    
    def updateFeed( self, feedId: int, feedDets: dict ):
        self._dbDriver.updateFeed( feedId, feedDets )

    def listFeeds( self, userId ):
        for indivFeed in self._dbDriver.listFeeds( userId ):
            yield indivFeed

    def storeToken( self, emailId, jwtToken ):
        return self._dbDriver.storeToken( emailId, jwtToken )
