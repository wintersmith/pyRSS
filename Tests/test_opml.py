import sys
import unittest

sys.path.append( '.' )

from rss import database
from rss.opml import parseOPML
from rss.feeds import listFeeds, checkFeed, updateFeed

testOPML = """<?xml version="1.0" encoding="UTF-8"?>
<opml version="1.0">
    <head>
        <title>subscriptions</title>
    </head>
    <body>
        <outline text="Geek" title="Geek">
            <outline type="rss" text="Netflix TechBlog" title="Netflix TechBlog" xmlUrl="http://techblog.netflix.com/feeds/posts/default" htmlUrl="https://medium.com/netflix-techblog?source=rss----2615bd06b42e---4"/>
            <outline type="rss" text="den of geek" title="den of geek" xmlUrl="http://news.google.com/news?hl=en&amp;gl=us&amp;q=den%20of%20geek&amp;um=1&amp;ie=UTF-8&amp;output=rss" htmlUrl="https://news.google.com/news/search/section/q/den%20of%20geek/den%20of%20geek?ned=us&amp;hl=en&amp;gl=US"/>
        </outline>
        <outline type="rss" text="furbo.org" title="furbo.org" xmlUrl="http://furbo.org/feed/" htmlUrl="https://furbo.org"/>
        <outline type="rss" text="Hips &amp; Curves" title="Hips &amp; Curves" xmlUrl="http://hipsandcurves.tumblr.com/rss" htmlUrl="http://hipsandcurves.tumblr.com/"/>
        <outline type="rss" text="c0t0d0s0.org" title="c0t0d0s0.org" xmlUrl="http://www.c0t0d0s0.org/feeds/index.rss2" htmlUrl="http://blog.moellenkamp.org/"/>
        <outline type="rss" text="Digital Photography School" title="Digital Photography School" xmlUrl="http://feeds.feedburner.com/DigitalPhotographySchool" htmlUrl="https://digital-photography-school.com"/>
        <outline type="rss" text="Scott Adams Blog" title="Scott Adams Blog" xmlUrl="http://dilbert.com/blog/entry.feed/" htmlUrl="http://blog.dilbert.com"/>
        <outline type="rss" text="eMercedesBenz" title="eMercedesBenz" xmlUrl="http://www.emercedesbenz.com/rss.xml" htmlUrl="http://www.emercedesbenz.com"/>
        <outline type="rss" text="MacRumors: Mac News and Rumors - Front Page" title="MacRumors: Mac News and Rumors - Front Page" xmlUrl="http://www.macrumors.com/macrumors.xml" htmlUrl="https://www.macrumors.com"/>
        <outline type="rss" text="BenzInsider.com - A Mercedes-Benz Fan Blog" title="BenzInsider.com - A Mercedes-Benz Fan Blog" xmlUrl="http://feeds.feedburner.com/BenzInsiderdotcom" htmlUrl="https://www.benzinsider.com"/>
        <outline type="rss" text="Concept Phones" title="Concept Phones" xmlUrl="http://feeds.feedburner.com/ConceptPhones?format=xml" htmlUrl="https://www.concept-phones.com"/>
        <outline type="rss" text="Marco.org" title="Marco.org" xmlUrl="http://www.marco.org/rss" htmlUrl="https://marco.org/"/>
        <outline type="rss" text="Neowin" title="Neowin" xmlUrl="http://feeds.neowin.net/neowin-all" htmlUrl="https://www.neowin.net/news/rss/"/>
        <outline type="rss" text="/Film" title="/Film" xmlUrl="http://feeds2.feedburner.com/slashfilm" htmlUrl="http://www.slashfilm.com"/>
        <outline type="rss" text="512 Pixels" title="512 Pixels" xmlUrl="http://feeds2.feedburner.com/forkbombr/rss2" htmlUrl="http://512pixels.net"/>
        <outline type="rss" text="The Verge -  All Posts" title="The Verge -  All Posts" xmlUrl="http://www.theverge.com/rss/index.xml" htmlUrl="https://www.theverge.com/"/>
        <outline type="rss" text="Tomb Raider Chronicles" title="Tomb Raider Chronicles" xmlUrl="http://www.tombraiderchronicles.com/news/index.xml" htmlUrl="http://www.tombraiderchronicles.com"/>
        <outline type="rss" text="Use Your Loaf" title="Use Your Loaf" xmlUrl="http://useyourloaf.com/blog/atom.xml" htmlUrl="https://useyourloaf.com/blog/"/>
        <outline type="rss" text="iLounge | All Things iPod, iPhone, iPad and Beyond" title="iLounge | All Things iPod, iPhone, iPad and Beyond" xmlUrl="http://feeds.feedburner.com/ilounge" htmlUrl="http://www.iLounge.com"/>
        <outline type="rss" text="inessential.com" title="inessential.com" xmlUrl="http://inessential.com/xml/rss.xml" htmlUrl="http://inessential.com/"/>
        <outline type="rss" text="io9" title="io9" xmlUrl="http://io9.com/index.xml" htmlUrl="http://io9.gizmodo.com"/>
        <outline type="rss" text="Ernie's Blog" title="Ernie's Blog" xmlUrl="http://www.ernestcline.com/blog/feed/" htmlUrl="http://www.ernestcline.com/blog"/>
        <outline type="rss" text="Ars Technica" title="Ars Technica" xmlUrl="http://feeds.arstechnica.com/arstechnica/index/" htmlUrl="https://arstechnica.com"/>
        <outline type="rss" text="BBC News - Home" title="BBC News - Home" xmlUrl="http://news.bbc.co.uk/rss/newsonline_uk_edition/front_page/rss.xml" htmlUrl="http://www.bbc.co.uk/news/"/>
        <outline type="rss" text="BBC News - Entertainment &amp; Arts" title="BBC News - Entertainment &amp; Arts" xmlUrl="http://newsrss.bbc.co.uk/rss/newsonline_uk_edition/entertainment/rss.xml" htmlUrl="http://www.bbc.co.uk/news/"/>
        <outline type="rss" text="Evil Avatar - News Items" title="Evil Avatar - News Items" xmlUrl="http://www.evilavatar.com/forums/external.php?forumid=2" htmlUrl="http://www.evilavatar.com/forums/"/>
        <outline type="rss" text="The Shape of Everything" title="The Shape of Everything" xmlUrl="http://gusmueller.com/blog/atom.xml" htmlUrl="http://shapeof.com/"/>
        <outline type="rss" text="Hacker News" title="Hacker News" xmlUrl="http://news.ycombinator.com/rss" htmlUrl="https://news.ycombinator.com/"/>
        <outline type="rss" text="MacSparky" title="MacSparky" xmlUrl="http://www.macsparky.com/blog/atom.xml" htmlUrl="https://www.macsparky.com/"/>
        <outline type="rss" text="Polygon -  All" title="Polygon -  All" xmlUrl="http://www.polygon.com/rss/index.xml" htmlUrl="https://www.polygon.com/"/>
        <outline type="rss" text="Shawn Blanc" title="Shawn Blanc" xmlUrl="http://shawnblanc.net/feed/" htmlUrl="https://shawnblanc.net"/>
        <outline type="rss" text="The Loop" title="The Loop" xmlUrl="http://www.loopinsight.com/feed/" htmlUrl="http://www.loopinsight.com"/>
        <outline type="rss" text="Alfred Blog" title="Alfred Blog" xmlUrl="http://blog.alfredapp.com/feed/" htmlUrl="https://www.alfredapp.com/blog/"/>
        <outline type="rss" text="Nikon Rumors" title="Nikon Rumors" xmlUrl="http://nikonrumors.com/feed" htmlUrl="https://nikonrumors.com"/>
        <outline type="rss" text="Uploads from ChezneyA" title="Uploads from ChezneyA" xmlUrl="http://api.flickr.com/services/feeds/photos_public.gne?id=10635438@N02&amp;lang=en-us&amp;format=rss_200" htmlUrl="http://www.flickr.com/photos/10635438@N02/"/>
        <outline type="rss" text="Blu-ray.com - Blu-ray Disc news" title="Blu-ray.com - Blu-ray Disc news" xmlUrl="http://www.blu-ray.com/rss/newsfeed.xml" htmlUrl="http://www.blu-ray.com"/>
        <outline type="rss" text="Blue's News" title="Blue's News" xmlUrl="http://bluesnews.com/news/news_1_0.rdf" htmlUrl="https://www.bluesnews.com"/>
        <outline type="rss" text="Cocoa with Love" title="Cocoa with Love" xmlUrl="http://feeds.feedburner.com/CocoaWithLove" htmlUrl="https://www.cocoawithlove.com/"/>
        <outline type="rss" text="Dark Horizons - General Feed" title="Dark Horizons - General Feed" xmlUrl="http://www.darkhorizons.com/feed.atom" htmlUrl="http://www.darkhorizons.com"/>
        <outline type="rss" text="ongoing by Tim Bray" title="ongoing by Tim Bray" xmlUrl="http://www.tbray.org/ongoing/ongoing.atom" htmlUrl="https://www.tbray.org/ongoing/"/>
        <outline type="rss" text="Panic Blog" title="Panic Blog" xmlUrl="http://www.panic.com/blog/feed/" htmlUrl="https://panic.com/blog"/>
        <outline type="rss" text="Ray Wenderlich" title="Ray Wenderlich" xmlUrl="http://www.raywenderlich.com/feed/" htmlUrl="https://www.raywenderlich.com"/>
        <outline type="rss" text="Daring Fireball" title="Daring Fireball" xmlUrl="http://daringfireball.net/feeds/main" htmlUrl="https://daringfireball.net/"/>
        <outline type="rss" text="Apple World Today" title="Apple World Today" xmlUrl="http://www.appleworld.today/?format=rss" htmlUrl="https://www.appleworld.today/"/>
        <outline type="rss" text="ComicsVerse" title="ComicsVerse" xmlUrl="http://feeds.feedburner.com/Comicsverse?format=xml" htmlUrl="https://comicsverse.com"/>
        <outline type="rss" text="Pocket Tactics" title="Pocket Tactics" xmlUrl="http://www.pockettactics.com/feed/" htmlUrl="https://www.pockettactics.com/"/>
        <outline type="rss" text="Six Colors" title="Six Colors" xmlUrl="http://feedpress.me/sixcolors?type=xml" htmlUrl="https://www.sixcolors.com/"/>
        <outline type="rss" text="TVWise" title="TVWise" xmlUrl="http://www.tvwise.co.uk/feed/" htmlUrl="https://www.tvwise.co.uk"/>
        <outline type="rss" text="The Sweet Setup" title="The Sweet Setup" xmlUrl="http://thesweetsetup.com/feed" htmlUrl="https://thesweetsetup.com"/>
        <outline type="rss" text="And now it’s all this" title="And now it’s all this" xmlUrl="http://www.leancrew.com/all-this/feed/" htmlUrl="http://leancrew.com/all-this/"/>
        <outline type="rss" text="Gollancz blog" title="Gollancz blog" xmlUrl="http://www.gollancz.co.uk/feed/" htmlUrl="https://www.gollancz.co.uk"/>
        <outline type="rss" text="All of Time and Space . . ." title="All of Time and Space . . ." xmlUrl="http://blog.sfgateway.com/index.php/feed/" htmlUrl="https://blog.sfgateway.com"/>
        <outline type="rss" text="Swift Blog - Apple Developer" title="Swift Blog - Apple Developer" xmlUrl="http://developer.apple.com/swift/blog/news.rss" htmlUrl="https://developer.apple.com/swift/blog/"/>
        <outline type="rss" text="Collider" title="Collider" xmlUrl="http://www.collider.com/rss.asp" htmlUrl="http://collider.com"/>
    </body>
</opml>
"""

localDB = database.Database( None, None, '../pyRSS.db' )
localDB.connect()


class TestHTTP( unittest.TestCase ):

    def test_parseFile( self ):
        for fText, fTitle, fXmlUrl, fHtmlUtl in parseOPML( testOPML ):
            if not ( fXmlUrl is None or fHtmlUtl is None ):
                pass


if __name__ == '__main__':
    unittest.main()