import sys
import unittest

sys.path.append( '.' )

from rss import http


class TestHTTP( unittest.TestCase ):

    def test_isHttpPass( self ):
        urlObj = http.URL( 'http://mytest.url' )
        self.assertTrue( urlObj.isHTTP() )

    def test_isHttpFail( self ):
        urlObj = http.URL( 'https://mytest.url' )
        self.assertFalse( urlObj.isHTTP() )

    def test_isHttpsPass( self ):
        urlObj = http.URL( 'https://mytest.url' )
        self.assertTrue( urlObj.isHTTPS() )

    def test_isHttpsFail( self ):
        urlObj = http.URL( 'http://mytest.url' )
        self.assertFalse( urlObj.isHTTPS() )

    def test_queryStringToDict( self ):
        self.assertTrue( http.queryStringToDict( 'a=b?c=d?e=f' ) == { 'a': 'b', 'c': 'd', 'e': 'f' } )


if __name__ == '__main__':
    unittest.main()
